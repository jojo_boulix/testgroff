#include <locale.h>
#include <stdio.h>
#include <stdlib.h>

extern char **environ;

int main() {

  setenv("TEST", "OK", 1);
  
  for(int i=0; environ[i] ; i++) fprintf(stderr, "%s\n", environ[i]);

  char *locale = setlocale (LC_ALL, "");
  if(locale) {
    printf("Locale: %s\n", locale);
  } else {
    printf("Locale: NULL\n");
  }
}
